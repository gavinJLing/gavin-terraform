




resource "aws_s3_bucket" "bucket" {
  bucket            = "${lower(var.bucket_name)}"
  acl               = "private"
  region            = "${var.region}"
  force_destroy     = "false"

  versioning {
    enabled = "false"
  }
  
  #logging {
  #  target_bucket = "${var.log_bucket_id}"
  #  target_prefix = "log/"
  #}

  # change this to CMK encryption
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }
  tags = "${merge(map("purpose","SourceData to be ingested to the data store is placed here..."), var.tags)}"
}



resource "aws_s3_bucket_public_access_block" "block" {
  bucket = "${aws_s3_bucket.bucket.id}"
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true 

}

resource "aws_s3_bucket_policy" "RoleBucketPolicy" {
  #count  = "0"
  bucket = "${aws_s3_bucket.bucket.id}"
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id" : "${var.bucket_name}-bucket-access-policy",
  "Statement": [ 
      {
        "Effect": "Allow", 
        "Principal": "*",
        "Action": "s3:*",
        "Resource": [
          "arn:aws:s3:::${aws_s3_bucket.bucket.id}",
          "arn:aws:s3:::${aws_s3_bucket.bucket.id}/*"
        ]
      }
  ]
}
 POLICY
}




