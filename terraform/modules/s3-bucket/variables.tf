variable "bucket_name" {
  description = "Name of the bucket in question"
  
}











variable "region" {
  description = "AWS region to deploy to"
}


variable "environment" {
  description = "Environment tag - dev/test/prod. Should match CI_ENVIRONMENT_NAME"
}


variable "tags" {
   description = "General tags to assign to the resource."
   type = "map"
}


