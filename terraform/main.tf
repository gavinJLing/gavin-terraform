# Top level Terraform project....
# Bring together various high level system components.

##########################
##  IAM Roles  Components 
##########################
#
module "awsRoles" {
    source = "./modules/iam_roles"
    role_name         = "engineer"
    role_description  = "An engineering level role that can do manipulate source data"

    environment         = "${var.environment}"
    tags                = "${var.tags}"
}
 



##########################
##  S3 source data storage  
##########################
#
/*
module "s3-bucket" {
  source = "./modules/s3-bucket"

  region              = "${var.region}"
  bucket_name         = "${var.project_name}-source"             
  
  environment         = "${var.environment}"
  tags                = "${var.tags}"
} 
*/




##########################
##  VPC
##########################
#
# wip


